from flask import Flask

from airport_server import Airport
from app_utils.api_handler import APIHandler

app = Flask(__name__)


api_handler = APIHandler()
server_thread = None


@app.errorhandler(Exception)
def handle_exception(error):
    error_message = str(error)
    return api_handler.get_response(error_message, 500)


@app.route("/run", methods=["PUT"])
def run_server():
    global api_handler, server_thread
    message, status_code = api_handler.run(Airport())
    if status_code == 200:
        server_thread = api_handler.get_server_thread()
        server_thread.start()

    return message, status_code


@app.route("/stop", methods=["PUT"])
def stop_server():
    global api_handler, server_thread
    message_content, status_code = api_handler.stop()
    if status_code == 200:
        server_thread.join()
        api_handler.server = None
        server_thread = None
    
    return message_content, status_code


@app.route("/pause", methods=["PUT"])
def pause_server():
    global api_handler
    message_content, status_code = api_handler.pause()
    return message_content, status_code


@app.route("/resume", methods=["PUT"])
def resume_server():
    global api_handler
    message_content, status_code = api_handler.resume()
    return message_content, status_code

@app.route("/uptime")
def get_server_uptime():
    global api_handler
    message_content, status_code = api_handler.uptime()
    return message_content, status_code

@app.route("/number-of-planes")
def get_number_of_planes():
    global api_handler
    message_content, status_code = api_handler.number_of_planes()
    return message_content, status_code

@app.route("/collisions")
def get_collisions():
    global api_handler
    message_content, status_code = api_handler.collisions()
    return message_content, status_code

@app.route("/landed")
def get_completed_flights():
    global api_handler
    message_content, status_code = api_handler.completed_flights()
    return message_content, status_code

@app.route("/planes-id")
def get_all_planes_id():
    global api_handler
    message_content, status_code = api_handler.planes_id()
    return message_content, status_code

@app.route("/plane-info/<id>")
def get_plane_info(id):
    global api_handler
    message_content, status_code = api_handler.plane_info(id)
    return message_content, status_code



if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
