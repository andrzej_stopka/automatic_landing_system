"""
This module contains the tests for the Plane class defined in utilities/plane.py.
"""


import unittest
import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.join(current_dir, "..")
sys.path.append(project_dir)

from server_utils.plane import Plane


class TestPlane(unittest.TestCase):
    """
    A collection of unit tests for the Plane class in the automatic landing system.
    """
    def setUp(self):
        self.plane = Plane()

    def test_not_done_avoid(self):
        """
        Test for `circle_over_airport` function.

        Ensures that the plane does not complete an avoidance maneuver when it is not yet finished
        or the plane has not been instructed to perform the avoidance maneuver.
        """
        self.plane.altitude = 1500
        avoid_completed = self.plane.circle_over_airport(self.plane.horizontal_position, self.plane.vertical_position, self.plane.altitude + 30)
        self.assertFalse(avoid_completed)

    def test_done_avoid(self):
        """
        Test for `circle_over_airport` function.

        Ensures that the plane completes an avoidance maneuver when it is finished.
        """
        self.plane.altitude = 1500
        avoid_completed = self.plane.circle_over_airport(self.plane.horizontal_position, self.plane.vertical_position, self.plane.altitude + 9)
        self.assertTrue(avoid_completed)

    def test_get_corridor_direction(self):
        """
        Test for `get_corridor_direction` function.

        Test that the plane correctly determines its flight direction towards a given corridor.
        """
        corridor_horizontal = 500
        corridor_vertical = 0
        corridor_altitude = 500

        self.plane.horizontal_position = -4000
        self.plane.vertical_position = 3451
        self.plane.altitude = 932

        self.plane.get_corridor_direction(corridor_horizontal, corridor_vertical, corridor_altitude)
        self.assertEqual(self.plane.flight_direction, "ESD")

        self.plane.horizontal_position = 4000
        self.plane.vertical_position = -3451
        self.plane.altitude = 300

        self.plane.get_corridor_direction(corridor_horizontal, corridor_vertical, corridor_altitude)
        self.assertEqual(self.plane.flight_direction, "WNU")

    def test_not_done_fly_to_corridor(self):
        """
        Test for `fly_to_corridor` function.

        Test that the plane does not reach a given corridor when it is not yet in it.
        """
        corridor_horizontal = 500
        corridor_vertical = 0
        corridor_altitude = 500

        self.plane.horizontal_position = corridor_horizontal + 10
        self.plane.vertical_position = corridor_vertical + 10
        self.plane.altitude = corridor_altitude + 10
        self.plane.get_corridor_direction(corridor_horizontal, corridor_vertical, corridor_altitude)
        in_corridor = self.plane.fly_to_corridor(corridor_horizontal, corridor_vertical, corridor_altitude)
        self.assertFalse(in_corridor)

    def test_done_fly_to_corridor(self):
        """
        Test for `fly_to_corridor` function.

        Test that the plane reaches a given corridor when it is already in it.
        """
        corridor_horizontal = 500
        corridor_vertical = 0
        corridor_altitude = 500

        self.plane.horizontal_position = corridor_horizontal + 5
        self.plane.vertical_position = corridor_vertical - 6.55
        self.plane.altitude = corridor_altitude + 6
        self.plane.get_corridor_direction(corridor_horizontal, corridor_vertical, corridor_altitude)
        in_corridor = self.plane.fly_to_corridor(corridor_horizontal, corridor_vertical, corridor_altitude)
        self.assertTrue(in_corridor)

    def test_get_landing_direction(self):
        """
        Test for the `get_landing_direction` function.

        It tests whether the method sets the correct flight direction based on the landing horizontal position.
        """
        landing_horizontal = 50
        self.plane.horizontal_position = 500
        self.plane.get_landing_direction(landing_horizontal)
        self.assertEqual(self.plane.flight_direction, "W")

        landing_horizontal = -50
        self.plane.horizontal_position = -500
        self.plane.get_landing_direction(landing_horizontal)
        self.assertEqual(self.plane.flight_direction, "E")

    def test_not_done_fly_to_landing_point(self):
        """
        Test case for the `fly_to_landing_point` function.

        It tests whether the method returns False if the plane has not yet reached the landing point.
        """
        landing_horizontal = 50
        self.plane.horizontal_position = 421.25
        self.plane.altitude = 42.4
        self.plane.get_landing_direction(landing_horizontal)
        landed = self.plane.fly_to_landing_point(landing_horizontal)
        self.assertFalse(landed)

    def test_done_fly_to_landing_point(self):
        """
        Test case for the `fly_to_landing_point` function.

        It tests whether the method returns True if the plane has reached the landing point.
        """
        landing_horizontal = -50
        self.plane.horizontal_position = -52.49
        self.plane.altitude = 6.99
        self.plane.get_landing_direction(landing_horizontal)
        landed = self.plane.fly_to_landing_point(landing_horizontal)
        self.assertTrue(landed)


if __name__ == '__main__':
    unittest.main()
