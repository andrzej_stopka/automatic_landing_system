#!/bin/bash


# sudo apt-get install curl -y
# sudo apt-get install jq -y


handle_error() {
    echo "Error: $1" >&2
    exit 1
}

send_get_request() {
    method=$1
    url=$2
    response=$(curl -s -X "$method" "$url") || handle_error "Unable to send HTTP GET request"
    echo "Response: $response"
}

# Check if cURL is installed
command -v curl >/dev/null 2>&1 || handle_error "cURL not installed"
# Check if jq is installed
command -v jq > /dev/null 2>&1 || handle_error "jq not installed"


# Create and activate the virtual environment
python3 -m venv .venv || handle_error "Cannot create a virtual environment"
source .venv/bin/activate || handle_error "Cannot activate virtual environment"


# Run the app.py file and wait until it starts
python app.py &
app_pid=$!

# Wait for the server to be ready
while ! curl -s http://localhost:5000/ >/dev/null; do
    echo "Waiting for server to start..."
    sleep 1
done

echo "Server is ready."


# Send HTTP request to stop the server (check if you can stop server if it is not running)
send_get_request "PUT" "http://127.0.0.1:5000/stop"

sleep 1

# Send HTTP request to start the server
send_get_request "PUT" "http://localhost:5000/run"


sleep 1
# Send HTTP request to pause the server
send_get_request "PUT" "http://localhost:5000/pause"

sleep 1
# Send HTTP request to resume the server
send_get_request "PUT" "http://localhost:5000/resume"

sleep 1

# Run 120_connections test 

python3 tests/120_connections_test.py > /dev/null 2>&1 &


sleep 1
# Send HTTP request to get server uptime
send_get_request "GET" "http://localhost:5000/uptime"

sleep 1
# Send HTTP request to get number of planes 
send_get_request "GET" "http://localhost:5000/number-of-planes"

sleep 1
# Send HTTP request to get info about collisions (it should be info about no collisions)
send_get_request "GET" "http://localhost:5000/collisions"

sleep 1
# Send HTTP request to get info about landed planes (it could be info about no landed planes)
send_get_request "GET" 'http://localhost:5000/landed'

sleep 1
# Send HTTP request to get list of planes id 
send_get_request "GET" "http://localhost:5000/planes-id"


sleep 1

# Get JSON Array length
length=$(echo "$response" | jq '.message | length')

# Draw an index from 0 to the length of the array - 1
random_index=$((RANDOM % length))

# Get the item with the drawn index
random_plane_id=$(echo "$response" | jq --argjson index "$random_index" '.message[$index]')

# Remove quotes from random_plane_id to use it in url
random_plane_id=$(echo "$random_plane_id" | tr -d '"')


# Send HTTP request to get info about random plane
send_get_request "GET" "http://localhost:5000/plane-info/$random_plane_id"

# Stop the app.py and 120_connections_test.py processes
pkill -f "app.py"
pkill -f "120_connections_test.py"









