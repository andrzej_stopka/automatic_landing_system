import threading
import subprocess
import os
import sys


def run_client():
    """
    Function to run a client subprocess.
    """
    current_dir = os.path.dirname(os.path.abspath(__file__))
    project_dir = os.path.join(current_dir, "..")
    client_file = os.path.join(project_dir, "client.py")
    subprocess.call([sys.executable, client_file])


for i in range(120):
    thread = threading.Thread(target=run_client)
    thread.start()
