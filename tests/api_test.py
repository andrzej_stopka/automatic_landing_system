from typing import Generator
import pytest
from playwright.sync_api import Playwright, APIRequestContext
import time



@pytest.fixture(scope="session")
def api_request_context(playwright: Playwright) -> Generator[APIRequestContext, None, None]:
    request_context = playwright.request.new_context(base_url="http://localhost:5000")
    yield request_context
    request_context.dispose()


@pytest.mark.parametrize("endpoint", ["run", "pause", "resume", "uptime", "number-of-planes", "collisions", "landed", "planes-id"])
def test_endpoint_server(api_request_context: APIRequestContext, endpoint: str) -> None:
    if endpoint in ["run", "pause", "resume", "stop"]:
        request = api_request_context.put(f"/{endpoint}")
    else:
        request = api_request_context.get(f"/{endpoint}")

    assert request.ok
    time.sleep(1)
    


   

