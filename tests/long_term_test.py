import subprocess
import sys
import os
import random
import threading
import time

def run_client():
    """
    Function to run a client subprocess.
    """
    current_dir = os.path.dirname(os.path.abspath(__file__))
    project_dir = os.path.join(current_dir, "..")
    client_file = os.path.join(project_dir, "client.py")
    subprocess.call([sys.executable, client_file])


while True:
    for i in range(random.randint(0, 5)):
        thread = threading.Thread(target=run_client)
        thread.start()
    time.sleep(random.randint(0, 10))
