import unittest
import functools
from unittest.mock import MagicMock
import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.join(current_dir, "..")
sys.path.append(project_dir)

from airport_server import Airport
from server_utils.flight_data import FlightData
from server_utils.collision_detector import CollisionDetector



class TestAirport(unittest.TestCase):
    """
    Test cases for the Airport class defined in airport_server.py.
    """

    def setUp(self):
        self.airport = Airport()
        self.mock_socket = MagicMock(name="socket")
        self.plane = {"horizontal": 5000, "vertical": 3000, "altitude": 3000, "fuel": 10800}
        self.flight_data = self.airport._create_flight_data_object(self.mock_socket, self.plane)

    def close_socket(func):
        """
        Decorator that closes the socket after the function is executed.
        """
        @functools.wraps(func)
        def wrapper(self):
            func(self)
            self.airport.sock.close()
        return wrapper

    @close_socket
    def test_create_flight_data_object(self):
        """
        Test case for `create_flight_data_object` method.

        Checks whether the flight data object created by the method is of the FlightData class and whether the
        number of flights in the airport object has been increased by 1.
        """
        plane = {"horizontal": 1000, "vertical": 2000, "altitude": 3000, "fuel": 4000}
        flight_data = self.airport._create_flight_data_object(self.mock_socket, plane)
        self.assertIsInstance(flight_data, FlightData)
        self.assertEqual(len(self.airport.flights), 2)   # 2 due to self.flight_data in setUp

    @close_socket
    def test_designate_guidance_if_circle_closed_corridors(self):
        """
        Test case for `designate_guidance_if_circle` method when both corridors are closed.

        Checks whether the guidance parameter of the flight data object has been kept "Circle".
        """
        self.airport.area.corridor1.open = False
        self.airport.area.corridor2.open = False
        self.airport._designate_guidance_if_circle(self.flight_data)
        self.assertEqual(self.flight_data.guidance, "Circle")

    @close_socket
    def test_designate_guidance_if_circle_corridor1_open(self):
        """
        Test case for `designate_guidance_if_circle` method when corridor1 is open.

        Checks whether the guidance parameter of the flight data object has been set to "Corridor1".
        """
        self.airport.area.corridor2.open = False
        self.airport._designate_guidance_if_circle(self.flight_data)
        self.assertEqual(self.flight_data.guidance, "Corridor1")

    @close_socket
    def test_designate_guidance_if_circle_corridor2_open(self):
        """
        Test case for `designate_guidance_if_circle` method when corridor2 is open.

        Checks whether the guidance parameter of the flight data object has been set to "Corridor2".
        """
        self.airport.area.corridor1.open = False
        self.airport._designate_guidance_if_circle(self.flight_data)
        self.assertEqual(self.flight_data.guidance, "Corridor2")

    @close_socket
    def test_designate_guidance_if_circle_to_circle_avoid(self):
        """
        Test for `designate_guidance_if_circle` function when the plane is
        circling and must avoid collision with another plane.

        Ensures that the guidance is set to 'Circle Avoid' and the altitude is
        changed by 30 units.
        """
        plane = {"horizontal": self.flight_data.horizontal_position - 40,
                    "vertical": self.flight_data.vertical_position + 30,
                    "altitude": self.flight_data.altitude - 36,
                    "fuel": 4000}
        self.airport._create_flight_data_object(self.mock_socket, plane)
        current_self_flight_data_circling_altitude = self.flight_data.circling_parameters["altitude"]
        self.airport._designate_guidance_if_circle(self.flight_data)
        self.assertEqual(self.flight_data.guidance, "Circle Avoid")
        self.assertEqual(self.flight_data.circling_parameters["altitude"], current_self_flight_data_circling_altitude + 30)

    @close_socket
    def test_designate_guidance_if_circle_no_avoid(self):
        """
        Test for `designate_guidance_if_circle` function when the plane is
        circling and there is no need to avoid collision.

        Ensures that the guidance is set to 'Circle' and the circling altitude is not modified.
        """
        plane = {"horizontal": self.flight_data.horizontal_position - 40,
                        "vertical": self.flight_data.vertical_position + 30,
                        "altitude": self.flight_data.altitude - 51,
                        "fuel": 4000}
        self.airport._create_flight_data_object(self.mock_socket, plane)
        current_self_flight_data_circling_altitude = self.flight_data.circling_parameters["altitude"]
        self.airport.area.corridor1.open = False
        self.airport.area.corridor2.open = False
        self.airport._designate_guidance_if_circle(self.flight_data)
        self.assertEqual(self.flight_data.guidance, "Circle")
        self.assertEqual(self.flight_data.circling_parameters["altitude"], current_self_flight_data_circling_altitude)

    @close_socket
    def test_planes_with_least_fuel(self):
        """
        Test for `FlightData.plane_with_least_fuel` static function.

        Ensures that the function returns a list of two planes with the lowest
        fuel among all flights.
        """
        lowest_fuel_plane = {"horizontal": 1000, "vertical": 2000, "altitude": 3000, "fuel": 400}
        highest_fuel_plane = {"horizontal": 1000, "vertical": 2000, "altitude": 3000, "fuel": 12000}
        lowest_fuel_data = self.airport._create_flight_data_object(self.mock_socket, lowest_fuel_plane)
        highest_fuel_data = self.airport._create_flight_data_object(self.mock_socket, highest_fuel_plane)
        two_planes_with_least_fuel = FlightData.plane_with_least_fuel(self.airport.flights)
        self.assertEqual(two_planes_with_least_fuel, [lowest_fuel_data, self.flight_data])

    @close_socket
    def test_guidance_to_command(self):
        """
        Test for `guidance_to_command` function.

        Ensures that the function returns a dictionary with the appropriate command
        depending on the guidance of the flight.
        """
        self.flight_data.guidance = "Circle"
        command = self.airport._guidance_to_command(self.flight_data)
        self.assertEqual(command, {"[COMMAND]": self.flight_data.circling_parameters})

        self.flight_data.guidance = "Circle Avoid"
        command = self.airport._guidance_to_command(self.flight_data)
        self.assertEqual(command, {"[COMMAND]": self.flight_data.circling_parameters})

        self.flight_data.guidance = "Corridor1"
        command = self.airport._guidance_to_command(self.flight_data)
        self.assertEqual(command, {"[COMMAND]": {"guidance": self.flight_data.guidance,
                                    "horizontal": self.airport.area.corridor1.horizontal,
                                    "vertical": self.airport.area.corridor1.vertical,
                                    "altitude": self.airport.area.corridor1.altitude}})

    @close_socket
    def test_update_flight_data(self):
        """
        Test for `update_flight_data` function.

        Ensures that the function updates the flight data with the new values.
        """
        data = {"horizontal": 1111, "vertical": 2222, "altitude": 3333, "fuel": 4444}
        self.airport._update_flight_data(data, self.flight_data)
        self.assertEqual(self.flight_data.horizontal_position, 1111)
        self.assertEqual(self.flight_data.vertical_position, 2222)
        self.assertEqual(self.flight_data.altitude, 3333)
        self.assertEqual(self.flight_data.fuel, 4444)

    @close_socket
    def test_change_guidance_from_corridor_to_landing(self):
        """
        Test for `change_guidance_from_corridor_to_landing function.

        Ensures that the function updates the flight data guidance to landing. If the previous guidance was corridor1, the function
        updates to landing1 and so on.
        """
        self.flight_data.guidance = "Corridor2"
        self.airport._change_guidance_from_corridor_to_landing(self.flight_data)
        self.assertEqual(self.flight_data.guidance, "Landing2")

    @close_socket
    def test_alt_below_zero_collision(self):
        """
        Test for `altitude_below_zero_collision` function in CollisionDetector class.

        Ensures that the function correctly detects a collision when a flight's altitude is below zero.
        """
        self.flight_data.altitude = -2
        below_zero_collision = CollisionDetector.altitude_below_zero_collision(self.airport.flights)
        self.assertTrue(below_zero_collision)

        self.flight_data.altitude = 0
        below_zero_collision = CollisionDetector.altitude_below_zero_collision(self.airport.flights)
        self.assertFalse(below_zero_collision)

    @close_socket
    def test_planes_collision(self):
        """
        Test for `planes_collision` function in CollisionDetector class.

        Ensures that the function correctly detects a collision when two planes are too close to each other.
        """
        data = {"horizontal": 4000, "vertical": 2200, "altitude": 2500, "fuel": 4444}
        self.airport._update_flight_data(data, self.flight_data)
        another_plane = {"horizontal": data["horizontal"] - 5, "vertical": data["vertical"] + 5, "altitude": data["altitude"] + 3, "fuel": 4444}
        self.airport._create_flight_data_object(self.mock_socket, another_plane)
        planes_collision = CollisionDetector.planes_collision(self.airport.flights)
        self.assertTrue(planes_collision)

    @close_socket
    def test_no_planes_collision(self):
        """
        Test for `planes_collision` function in CollisionDetector class.

        Ensures that the function correctly returns False when there is no collision between planes.
        """
        data = {"horizontal": 4000, "vertical": 2200, "altitude": 2500, "fuel": 4444}
        self.airport._update_flight_data(data, self.flight_data)
        another_plane = {"horizontal": data["horizontal"] - 5, "vertical": data["vertical"] + 5, "altitude": data["altitude"] + 11, "fuel": 4444}
        self.airport._create_flight_data_object(self.mock_socket, another_plane)
        planes_collision = CollisionDetector.planes_collision(self.airport.flights)
        self.assertFalse(planes_collision)

    @close_socket
    def test_fuel_run_out(self):
        """
        Test for fuel_has_ran_out_collision function in CollisionDetector class.

        Ensures that the function correctly detects when a plane has run out of fuel.
        """
        self.flight_data.fuel = 0
        fuel_collision = CollisionDetector.fuel_has_ran_out_collision(self.airport.flights)
        self.assertTrue(fuel_collision)


if __name__ == "__main__":
    unittest.main()
