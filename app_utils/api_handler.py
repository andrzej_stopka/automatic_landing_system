import threading
import time
from flask import jsonify

class APIHandler:

    def __init__(self):
        self.server = None

    def get_server_thread(self):
        server_thread = threading.Thread(target=self.server.run)
        return server_thread
    
    def get_response(self,message_content, status_code):
        return jsonify({"message": message_content}), status_code
    
    def run(self, server):
        if not self.server:
            self.server = server
            self.start_time = time.time()
            return self.get_response("Server has been started", 200)
        else:
            return self.get_response("Server is already running", 400)

    def stop(self):
        if not self.server:
            return self.get_response("Server hasn't been started", 400)
        else:
            self.server.state = "OFF"
            return self.get_response("Server has been stopped", 200)

    def check_if_server_is_off(func):
        def wrapper(self, *args, **kwargs):
            if not self.server or self.server.state == "OFF":
                return self.get_response("Server is already off", 400)
            else:
                return func(self, *args, **kwargs)
        return wrapper

    @check_if_server_is_off
    def pause(self):
        if self.server.state == "ON":
            self.server.state = "PAUSE"
            return self.get_response("Server has been paused", 200)
        elif self.server.state == "PAUSE":
            return self.get_response("Server is already paused", 400)

    @check_if_server_is_off
    def resume(self):
        if self.server.state == "ON":
            return self.get_response("Server is not paused", 400)
        elif self.server.state == "PAUSE":
            self.server.state = "ON"
            return self.get_response("Server has been resumed", 200)

    @check_if_server_is_off  
    def uptime(self):
        current_time = time.time()
        uptime_seconds = current_time - self.start_time
        uptime = time.strftime("%H:%M:%S", time.gmtime(uptime_seconds))
        return self.get_response(f"Server uptime: {uptime}", 200)
    
    @check_if_server_is_off
    def number_of_planes(self):
        with self.server.flights_lock:
            number_of_planes = len(self.server.flights)
        return self.get_response(f"Number of planes: {number_of_planes}", 200)
    
    @check_if_server_is_off
    def collisions(self):
        collisions = self.server.dh.get_collisions()
        if collisions == []:
            return self.get_response("No collisions", 200)
        return self.get_response(collisions, 200)
    
    @check_if_server_is_off
    def completed_flights(self):
        number_of_completed_flights = self.server.completed_flights
        completed_flights_info = self.server.dh.get_landed_planes()
        if completed_flights_info == []:
            return self.get_response("No completed flights", 200)
        return jsonify({"message": {"completed_flights": number_of_completed_flights, "info": completed_flights_info}}), 200
        
    @check_if_server_is_off
    def planes_id(self):
        planes_id = []
        with self.server.flights_lock:
            for flight in self.server.flights:
                planes_id.append(flight.plane_id)
                
        if planes_id == []:
            return self.get_response("No planes", 200)
        return self.get_response(planes_id, 200)
    
    @check_if_server_is_off
    def plane_info(self, plane_id):
        plane = None
        with self.server.flights_lock:
            for flight in self.server.flights:
                if flight.plane_id == plane_id:
                    plane = flight
                    break
        if not plane:
            return self.get_response(f"Plane with id {plane_id} doesn't exist", 400)
        horizontal_position = plane.horizontal_position
        vertical_position = plane.vertical_position
        altitude = plane.altitude
        fuel = plane.fuel
        entry_time = plane.entry_time
        guidance = plane.guidance
        plane_info = {"horizontal_position": horizontal_position, "vertical_position": vertical_position, "altitude": altitude, "fuel": fuel
                        , "entry_time": entry_time, "guidance": guidance}
        return self.get_response(plane_info, 200)
        




    