import logging
import sqlite3
from datetime import datetime

class DatabaseHandler(logging.Handler):
    """
    A custom logging handler that logs messages to an SQLite database.

    Args:
        db_file (str): The path to the SQLite database file. Defaults to 'logging_database/als_logs.db'.

    Attributes:
        logger (logging.Logger): The logger instance that uses this handler.

    """

    def __init__(self, db_file="logging_database/als_logs.db"):
        super().__init__()
        self.db_file = db_file
        self.connect_to_database()
        self._truncate_tables()
        self.create_tables()
        self.logger = self.create_logger()

    def connect_to_database(self):
        self.conn = sqlite3.connect(self.db_file)

    def create_tables(self):
        cursor = self.conn.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS logs
            (id INTEGER PRIMARY KEY AUTOINCREMENT,
            timestamp TEXT,
            level TEXT,
            message TEXT)''')
        
        cursor.execute('''CREATE TABLE IF NOT EXISTS collisions
            (id INTEGER PRIMARY KEY AUTOINCREMENT,
            timestamp TEXT,
            collision_type TEXT,
            plane_id TEXT,
            plane_horizontal INT,
            plane_vertical INT,
            plane_altitude INT,
            plane_fuel INT,
            involved_plane_id TEXT)''')
        
        cursor.execute('''CREATE TABLE IF NOT EXISTS landed_planes
            (id INTEGER PRIMARY KEY AUTOINCREMENT,
            plane_id TEXT,
            entry_time TEXT,
            landing_time TEXT,
            flight_duration TEXT)''')
            
        self.conn.commit()

    def _truncate_tables(self):
        """
        Deletes all rows in tables.
        """
        try:
            cursor = self.conn.cursor()
            cursor.execute('DELETE FROM logs')
            cursor.execute('DELETE FROM collisions')
            cursor.execute('DELETE FROM landed_planes')
            self.conn.commit()
        except sqlite3.OperationalError:
            pass

    def create_logger(self):
        logger = logging.getLogger(__name__)
        logger.addHandler(self)
        logger.setLevel(logging.INFO)
        return logger

    def emit(self, record):
        """
        Writes a log record to the 'logs' table in the database.

        Args:
            record (logging.LogRecord): The log record to be written to the database.
        """
        self.connect_to_database()
        cursor = self.conn.cursor()
        timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        level = record.levelname
        message = record.getMessage()
        cursor.execute("INSERT INTO logs (timestamp, level, message) VALUES (?, ?, ?)", (timestamp, level, message))
        self.conn.commit()

    def upload_collision(self, record):
        """
        Writes a collision record to the 'collisions' table in the database.

        Args:
            record (dict): The collision record to be written to the database.
        """
        self.connect_to_database()
        cursor = self.conn.cursor()
        timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        collision_type = record["collision_type"]
        plane_id = record["plane_id"]
        plane_horizontal = record["plane_horizontal"]
        plane_vertical = record["plane_vertical"]
        plane_altitude = record["plane_altitude"]
        plane_fuel = record["plane_fuel"]
        involved_plane_id = record["involved_plane_id"]
        cursor.execute("""INSERT INTO collisions 
                        (timestamp, collision_type, plane_id, plane_horizontal, plane_vertical, plane_altitude, plane_fuel, involved_plane_id)
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?)""", 
                        (timestamp, collision_type, plane_id, plane_horizontal, plane_vertical, plane_altitude, plane_fuel, involved_plane_id))
        self.conn.commit()

    
    def get_collisions(self):
        self.connect_to_database()
        cursor = self.conn.cursor()
        cursor.row_factory = sqlite3.Row
        cursor.execute("SELECT timestamp, collision_type, plane_id, plane_horizontal, plane_vertical, plane_altitude, plane_fuel, involved_plane_id FROM collisions")
        rows = cursor.fetchall()

        collisions = []
        for row in rows:
            collision = dict(row)
            collisions.append(collision)
        
        return collisions
    
    def upload_landed_plane(self, record):
        """
        Writes a landed plane record to the 'landed_planes' table in the database.

        Args:
            record (dict): The landed plane record to be written to the database.
        """
        self.connect_to_database()
        cursor = self.conn.cursor()
        plane_id = record["plane_id"]
        entry_time = record["entry_time"]
        landed_time = record["landing_time"]
        flight_duration = record["flight_duration"]
        cursor.execute("""INSERT INTO landed_planes 
                            (plane_id, entry_time, landing_time, flight_duration)
                            VALUES (?, ?, ?, ?)""", 
                            (plane_id, entry_time, landed_time, flight_duration))
        self.conn.commit()

    def get_landed_planes(self):
        self.connect_to_database()
        cursor = self.conn.cursor()
        cursor.row_factory = sqlite3.Row
        cursor.execute("SELECT plane_id, entry_time, landing_time, flight_duration FROM landed_planes")
        rows = cursor.fetchall()

        landed_planes = []
        for row in rows:
            landed_plane = dict(row)
            landed_planes.append(landed_plane)
        
        return landed_planes