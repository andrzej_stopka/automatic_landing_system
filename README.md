# Automatic Landing System

Task in the mentoring program "ZaRączkę" in which I participate. Full guidelines in the assignments file, the rest of the assumptions were left to me.

[1#]  
The project that uses a number of technologies to create a simulation of collision-free air traffic management.
This project consists of a server, which acts as an airport, and clients, which mimic airplanes. The server is tasked with coordinating the clients' traffic in such a way as to ensure that they land safely and efficiently at the airport.
The project uses threads to process multiple requests from clients in parallel. Logging was also used to record the system's activities, making it easier to monitor and diagnose errors later.
A SQLite3 database was used to store logs. Communication between clients and the server is carried out via network sockets.
The project was also unit tested using the unittest module, which allowed early detection of errors and facilitated the debugging process. The full project guidelines can be found in the repository, where you can find more details on the implementation and operation of the automatic landing system.  

[2#]  
The project has been expanded with the addition of a Flask application that serves as a management for the server. The Flask app enhances the functionality of the automatic landing system by providing endpoints for controlling and monitoring the air traffic.
This application leverages the capabilities of the Flask framework to handle HTTP requests and acts as a web server. It offers various endpoints that enable users to interact with the server and access real-time information about the air traffic.
To showcase the functionality of the application in a concise manner, a Bash script has been created. This script provides a quick demonstration of the application's capabilities by automating the process of sending sample requests to its endpoints.
In order to support the expanded functionality provided by the Flask application, the existing SQLite3 database has been extended. Additional tables have been added to the database to handle the data related to the new endpoints. This extended database integration enables efficient data management and retrieval between the Flask application and the underlying storage.  

[3#]  
These updates have been implemented on a separate branch(RPC-API-Branch), while the main branch still retains the original Flask implementation.
The project has undergone further enhancements with the replacement of the Flask application by an XML-RPC server. This new server technology offers improved functionality and facilitates communication between the clients and the server.
To accommodate this change, the existing Bash script has been modified to work seamlessly with the XML-RPC server. It automates the process of sending sample requests to the server's endpoints, showcasing the capabilities of the system.
In addition, a new file named app_client has been created to streamline the handling of requests to the current server. This client application simplifies the interaction with the server and provides a convenient interface for users to send requests and receive responses.
These updates provide a more efficient and versatile solution for controlling and monitoring the air traffic within the automatic landing system. The documentation in the repository provides further details on the implementation and usage of the XML-RPC server and the app_client application.

**From now every refactoring and improvement is my idea**

[4#]
I created a simple visualization of airport and aircraft behavior using VPython. Unfortunately, the visualization is only available on a separate branch, where the web application is not available because the animation has to be executed on the main thread which I couldn't handle by creating a web application. In the visualization, it may appear that the planes are too close to each other, but this is because the plane objects in the animation are larger than in reality to be properly visible. However, the collision_detector class is responsible for tracking any collisions according to what is in the code.  

[5#]
I enabled containerisation by creating a Dockerfile and changing the host.

## Endpoints

**FLASK REST API**  

`PUT http://localhost:5000/run` - run the server on localhost  
`PUT http://localhost:5000/stop` - stop the server  
`PUT http://localhost:5000/pause` - pause the server    
`PUT http://localhost:5000/resume` - resume the server  
`GET http://localhost:5000/uptime` - get the server uptime  
`GET http://localhost:5000/number-of-planes` - get the number of planes in airspace  
`GET http://localhost:5000/collisions` - get the information about collisions  
`GET http://localhost:5000/landed` - get information about landed planes  
`GET http://localhost:5000/planes-id` - get the list of planes IDs that are currently in airspace  
`GET http://localhost:5000/plane-info/<plane_id>` - get information about specific plane by ID   

**XML-RPC API**

run the server on localhost

```
POST http://localhost:8000
Content-Type: text/xml

<?xml version="1.0" encoding="UTF-8"?>
<methodCall>
<methodName>run_server</methodName>
</methodCall>
```

stop the server

```
POST http://localhost:8000
Content-Type: text/xml

<?xml version="1.0" encoding="UTF-8"?>
<methodCall>
  <methodName>stop_server</methodName>
</methodCall>
```

pause the server

```
POST http://localhost:8000
Content-Type: text/xml

<?xml version="1.0" encoding="UTF-8"?>
<methodCall>
  <methodName>pause_server</methodName>
</methodCall>
```

resume the server

```
POST http://localhost:8000
Content-Type: text/xml

<?xml version="1.0" encoding="UTF-8"?>
<methodCall>
  <methodName>resume_server</methodName>
</methodCall>
```

get the server uptime

```
POST http://localhost:8000
Content-Type: text/xml

<?xml version="1.0" encoding="UTF-8"?>
<methodCall>
  <methodName>uptime</methodName>
</methodCall>
```

get the number of planes in airspace

```
POST http://localhost:8000
Content-Type: text/xml

<?xml version="1.0" encoding="UTF-8"?>
<methodCall>
  <methodName>number_of_planes</methodName>
</methodCall>
```

get the information about collisions

```
POST http://localhost:8000
Content-Type: text/xml

<?xml version="1.0" encoding="UTF-8"?>
<methodCall>
  <methodName>collisions</methodName>
</methodCall>
```

get information about landed planes

```
POST http://localhost:8000
Content-Type: text/xml

<?xml version="1.0" encoding="UTF-8"?>
<methodCall>
  <methodName>completed_flights</methodName>
</methodCall>
```

get the list of planes IDs that are currently in airspace

```
POST http://localhost:8000
Content-Type: text/xml

<?xml version="1.0" encoding="UTF-8"?>
<methodCall>
  <methodName>planes_id</methodName>
</methodCall>
```

get information about specific plane by ID

```
POST http://localhost:8000
Content-Type: text/xml

<?xml version="1.0" encoding="UTF-8"?>
<methodCall>
  <methodName>plane_info</methodName>
  <params>
    <param>
        <value><string><plane_id></string></value>
    </param>
   </params> 
</methodCall>
```

## Authors

- [@andrzej_stopka](https://gitlab.com/andrzej_stopka)

## 🔗 Links

[gitlab](https://gitlab.com/andrzej_stopka)

[linkedin](https://www.linkedin.com/in/andrzej-stopka/)

## 🚀 About Me

I'm a Python programmer who has been learning the language for the past 8 months. I'm currently participating in the ZaRączkę mentoring program, which has helped me to grow my skills and knowledge in Python. I'm passionate about software development and enjoy working on personal projects in my free time.

Feel free to contact me if you have any questions or would like to connect!

## Lessons Learned

During the development of this project, I learned a lot of valuable lessons that helped me become a better Python developer. Some of the key lessons I learned include:

The value of planning and breaking problems down into smaller parts: Throughout the development of this project, I realized how helpful it is to plan out a project ahead of time, especially when I'm not sure where to start. I found that taking the time to sketch out my ideas on paper helped me organize my thoughts and come up with a clear plan of action. Additionally, breaking problems down into smaller, more manageable parts made it easier for me to start tackling them one at a time.

How to build a server that can handle multiple connections: Building a server that can handle multiple connections was a key milestone for me in this project. I used the socket module to create a TCP/IP server, and I had to make sure that it could handle multiple connections simultaneously. This required me to learn more about threads and how they work in Python.

The importance of using threading and locking: As I started working on more complex projects, I realized how important it is to use threading and locking to avoid race conditions and other concurrency issues. In this project, I used the threading module to create multiple threads that could run concurrently, and I used locking to ensure that multiple threads didn't try to access the same data at the same time.

The importance of using a logging module: When I first started working on this project, I didn't understand the value of using a logging module. However, as I started writing more complex code and encountering more errors, I quickly realized how useful logging can be. It allowed me to easily track down bugs and errors, and it also helped me understand how my code was running.

Flask application development: I deepened my understanding of Flask and learned how to build a Flask application that serves as a management interface for the server.

Bash scripting basics: I acquired fundamental knowledge of Bash scripting, which allowed me to create a concise Bash script to demonstrate the functionality of the Flask application.

RPC-API, XML-RPC server, and XML structure: As the project evolved, I further expanded my skillset by learning about RPC-API and XML-RPC server development. I delved into the concepts of remote procedure calls (RPC) and mastered the creation of an XML-RPC server. Additionally, I gained proficiency in understanding and constructing XML structures, which played a crucial role in the communication between clients and the XML-RPC server.

VPython: I am aware that the VPython library will probably not be useful to me in the future, however, I necessarily wanted to create such a visualization, for which VPython seemed the best.

Docker: I have grasped the basics of Docker, how to build an image, run a container and what port mapping is.

The importance of code structure: Finally, I learned the importance of having a well-structured codebase. When I first started working on this project, my code was disorganized and difficult to navigate. However, as I continued to work on it, I realized that having a clear structure and organization is essential for writing maintainable and scalable code.

## Tech Stack

**App:**    Python:

- Flask
- threading
- time
- xmlrpc

**Server:** Python:

- socket
- threading
- logging
- sqlite3
- json
- time
- random
- datetime
- unittest
- vpython

**Client:** Python:

- socket
- threading
- json
- random
- time

**Other**  
- Bash
- Docker

## Run Locally

To run this project, you will need to have the following tools installed on your computer:

- Python 3.x
- Pip
- For a better view of the database you can install DB browser to sqlite

Once you have installed the necessary tools, you can follow these steps to install and run the project:

**Run without app (visualization)**

1. Clone the repository to your local machine using the command `https://gitlab.com/automation_landing_system/automatic_landing_system/-/tree/Visualization?ref_type=heads`.
2. Create a new virtual environment using the command `python -m venv myenv`
3. Install the required dependencies by running the following command: `pip install -r requirements.txt`
4. Run the server using the command `python airport_server.py`, you will be redirected to a web browser where the visualization where the visualization will be.
5. Run the client using the command `python client.py` or:
   1. Navigate to the test directory using the
      `cd test`
   2. Run one of tests using the command
      `python 120_connections_test.py` or
      `python long_term_test.py`
6. Just watch what happens 

That's it! The project should now be up and running on your local machine.

**Run Flask app**

1. Clone the repository to your local machine using the command `https://gitlab.com/automation_landing_system/automatic_landing_system/-/tree/Flask-API-Branch`.
2. Create a new virtual environment using the command `python -m venv myenv`
3. Activate the virtual environment using the command:
   For Windows:
   `myenv\Scripts\activate`
   For Unix or Linux:
   `source myenv/bin/activate`
4. Install the required dependencies by running the following command: `pip install -r requirements.txt`
5. Run the application using the command `python app.py`
6. Now you can interact with the application sending requests [all request examples in ##ENDPOINTS section]

That's it! The project should now be up and running on your local machine.

**Run XML RPC server**

1. Clone the repository from RPC-API-Branch to your local machine using the command `git clone https://gitlab.com/automation_landing_system/automatic_landing_system/-/tree/RPC-API-Branch`.
2. Create a new virtual environment using the command `python -m venv myenv`
3. Activate the virtual environment using the command:
   For Windows:
   `myenv\Scripts\activate`
   For Unix or Linux:
   `source myenv/bin/activate`
4. Run the application using the command `python app.py`
5. Now you can interact with the application sending requests [all request examples in ##ENDPOINTS section]
   or
   Run the application client using the `python app_client.py` command which facilitates sending requests

That's it! The project should now be up and running on your local machine.

**Run Flask or XML app by Docker** (It is unable for visualization)
1. Clone the repository from Flask-API-Branch or RPC-API-Branch to your local machine using the command `git clone https://gitlab.com/automation_landing_system/automatic_landing_system/-/tree/RPC-API-Branch` or `git clone https://gitlab.com/automation_landing_system/automatic_landing_system/-/tree/RPC-API-Branch`. 
2. In main directory create a docker image `docker build -t als .`(You need to have installed Docker)
3. Run docker image with port mapping `docker run -p 5000:5000 -p 54321:54321 -d als` (-d to run it in the background)
4. Now you can interact with the application sending requests [all request examples in ##ENDPOINTS section]

That's it! The project should now be up and running on your local machine.


## Running Tests

To run a unit tests, navigate to the tests directory in your command line interface and enter the following command:

`python airport_server_unittest.py`
or
`python plane_unittest.py`

(NOTE: ONLY UNIX OR LINUX)

To run a bash script test that demonstratesthe functionality navigate to the main directory in your command line interface and enter the following command:
`./scripts/test_app.sh`

## Feedback

I appreciate your feedback on my project! If you have any suggestions, feature requests, or bug reports, please let me know.

You can send your feedback to me by sending a private message on GitLab, or by email to andrzej.stopka1996@gmail.com.

Your feedback is important to me, and I use it to improve my project and make it better. Thank you for your support!

## Documentation

[Documentation](documentation.py)

## Demo


[DEMO](https://www.youtube.com/watch?v=JGlqlOD4TA0)



## Appendix

The work was done entirely by me, not using any tutorials or mentor help
