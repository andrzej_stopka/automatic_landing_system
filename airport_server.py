import socket
import threading
import time

from datetime import datetime

from server_utils.communication import send_json, recv_json
from server_utils.airport_area import AirportArea
from server_utils.flight_data import FlightData
from server_utils.collision_detector import CollisionDetector
from logging_database.database_handler import DatabaseHandler

class Airport:
    def __init__(self, host="0.0.0.0", port=54321):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.settimeout(0.5)
        self.state = "OFF"
        self.flights = []
        self.flights_lock = threading.Lock()
        self.area = AirportArea()
        self.dh = DatabaseHandler()
        self.corridors_lock = threading.Lock()
        self.completed_flights = 0
        self.completed_flights_lock = threading.Lock()

    

    def run(self):
        self.sock.bind((self.host, self.port))
        self.sock.listen()
        self.state = "ON"
        while self.state != "OFF":
            if self.state == "PAUSE":
                time.sleep(0.5)
                continue
            try:
                client_socket, _ = self.sock.accept()
                client_thread = threading.Thread(target=self.handle_client, args=(client_socket,))
                client_thread.start()
            except socket.timeout:
                pass
            collision_thread = threading.Thread(target=self.check_collisions)
            collision_thread.start()

        self.sock.close()
    def handle_client(self, client_socket):
        with self.flights_lock:
            permission = self._grant_permission()
            if not permission:
                send_json(client_socket, {"[PERMISSION]": "Denied"})
                client_socket.close()
                return
            else:
                send_json(client_socket, {"[PERMISSION]": "Allowed"})
                plane_details = recv_json(client_socket)
                flight_data = self._create_flight_data_object(client_socket, plane_details)

        while self.state != "OFF":
            if self.state == "PAUSE":
                time.sleep(0.5)
                continue
            self._designate_guidance_if_circle(flight_data)
            command = self._guidance_to_command(flight_data)
            send_json(client_socket, command)
            if flight_data.guidance == "Finished":
                self._finish_flight(flight_data)
                with self.flights_lock:
                    self.flights.remove(flight_data)
                    self.dh.logger.info(f"Current number of planes in the airspace: {len(self.flights)}")
                    break
            updated_data = recv_json(client_socket)
            self._update_flight_data(updated_data, flight_data)
            if updated_data.get("[MESSAGE]") == "Avoidance completed":
                self.dh.logger.info(f"[{flight_data.plane_id}] Avoidance has been completed")
                flight_data.guidance = "Circle"
            if updated_data.get("[MESSAGE]") == "In corridor":
                self._change_guidance_from_corridor_to_landing(flight_data)
            if updated_data.get("[MESSAGE]") == "Landed":
                flight_data.guidance = "Finished"
        client_socket.close()
        
    def _grant_permission(self):
        if len(self.flights) >= 100:
            self.dh.logger.info(f"Plane was denied to enter")
            return False
        else:
            self.dh.logger.info("Plane was allowed to enter")
            return True

    def _create_flight_data_object(self, client_socket, plane):
        plane_id = plane["plane id"]
        horizontal = plane["horizontal"]
        vertical = plane["vertical"]
        altitude = plane["altitude"]
        fuel = plane["fuel"]
        entry_time = datetime.now()
        flight_data = FlightData(client_socket, plane_id, horizontal, vertical, altitude, fuel, entry_time)
        self.flights.append(flight_data)
        return flight_data

    def _designate_guidance_if_circle(self, flight_data_object):
        if flight_data_object.guidance == "Circle":
            with self.flights_lock:
                for flight in self.flights:
                    if flight_data_object != flight and flight.guidance != "Circle Avoid":
                        if (abs(flight.vertical_position - flight_data_object.vertical_position) < 50
                                and abs(flight.horizontal_position - flight_data_object.horizontal_position) < 50
                                and abs(flight.altitude - flight_data_object.altitude) < 50):
                            flight_data_object.guidance = "Circle Avoid"
                            self.dh.logger.info(f"[{flight_data_object.plane_id}] Avoidance has been started")
                            current_altitude = flight_data_object.circling_parameters["altitude"]
                            desired_altitude = 30 if flight_data_object.altitude < 3500 else -30
                            flight_data_object.circling_parameters["altitude"] = current_altitude + desired_altitude
                            return

            with self.flights_lock:
                plane_with_least_fuel = FlightData.plane_with_least_fuel(self.flights)

            with self.area.corridor1.lock:
                with self.area.corridor2.lock:
                    if (not any(corridor.open is True for corridor in [self.area.corridor1, self.area.corridor2])
                                or flight_data_object not in plane_with_least_fuel):
                        flight_data_object.guidance = "Circle"
                        return

            with self.area.corridor1.lock:
                if self.area.corridor1.open is True:
                    flight_data_object.guidance = "Corridor1"

                    self.dh.logger.info(f"Plane {flight_data_object.plane_id} is going to corridor 1")

                    self.area.corridor1.open = False
                    return

            with self.area.corridor2.lock:
                if self.area.corridor2.open is True:
                    flight_data_object.guidance = "Corridor2"
                    self.dh.logger.info(f"Plane {flight_data_object.plane_id} is going to corridor 2")
                    self.area.corridor2.open = False
                    return

    def _guidance_to_command(self, flight_data_object):
        guidance = flight_data_object.guidance

        if guidance == "Circle" or guidance == "Circle Avoid":
            return {"[COMMAND]": flight_data_object.circling_parameters}

        if guidance == "Corridor1":
            return {"[COMMAND]": {"guidance": guidance,
                                        "horizontal": self.area.corridor1.horizontal,
                                        "vertical": self.area.corridor1.vertical,
                                        "altitude": self.area.corridor1.altitude}}
        if guidance == "Corridor2":
            return {"[COMMAND]": {"guidance": guidance,
                                        "horizontal": self.area.corridor2.horizontal,
                                        "vertical": self.area.corridor2.vertical,
                                        "altitude": self.area.corridor2.altitude}}

        if guidance == "Landing1":
            return {"[COMMAND]": {"guidance": guidance,
                                        "horizontal": self.area.landing_point1.horizontal,
                                        "vertical": self.area.landing_point1.vertical,
                                        "altitude": self.area.landing_point1.altitude}}

        if guidance == "Landing2":
            return {"[COMMAND]": {"guidance": guidance,
                                        "horizontal": self.area.landing_point2.horizontal,
                                        "vertical": self.area.landing_point2.vertical,
                                        "altitude": self.area.landing_point2.altitude}}

        if guidance == "Finished":
            return {"[COMMAND]": guidance}

    def _update_flight_data(self, data, flight_data_object):
        updated_horizontal = data["horizontal"]
        updated_vertical = data["vertical"]
        updated_altitude = data["altitude"]
        updated_fuel = data["fuel"]
        flight_data_object.update_data(updated_horizontal, updated_vertical, updated_altitude, updated_fuel)


    def _change_guidance_from_corridor_to_landing(self, flight_data_object):
        if flight_data_object.guidance == "Corridor1":
            with self.corridors_lock:
                self.area.corridor1.open = True
            flight_data_object.guidance = "Landing1"
            self.dh.logger.info(f"Plane {flight_data_object.plane_id} is landing on point 1")

        elif flight_data_object.guidance == "Corridor2":
            with self.corridors_lock:
                self.area.corridor2.open = True
            flight_data_object.guidance = "Landing2"
            self.dh.logger.info(f"Plane {flight_data_object.plane_id}  is landing on point 2")

    def _save_landed_plane_to_database(self, flight_data_object):
        plane_id = flight_data_object.plane_id
        entry_time = flight_data_object.entry_time
        landing_time = datetime.now()
        flight_duration = landing_time - entry_time
        str_entry_time = entry_time.strftime('%Y-%m-%d %H:%M:%S')
        str_landing_time = landing_time.strftime('%Y-%m-%d %H:%M:%S')
        str_flight_duration = str(flight_duration)
        record = {"plane_id": plane_id, "entry_time": str_entry_time, "landing_time": str_landing_time, "flight_duration": str_flight_duration}
        self.dh.upload_landed_plane(record)


    def _finish_flight(self, flight_data_object):
        self.dh.logger.info(f"Plane {flight_data_object.plane_id}  has landed, navigation for the plane has been completed")
        with self.completed_flights_lock:
            self.completed_flights += 1
            self.dh.logger.info(f"Number of completed flights: {self.completed_flights}")
        self._save_landed_plane_to_database(flight_data_object)
        client_socket = flight_data_object.client_socket
        client_socket.close()

    def check_collisions(self):
        time.sleep(1)
        with self.flights_lock:
            altitude_below_zero = CollisionDetector.altitude_below_zero_collision(self.flights)
            planes_collision = CollisionDetector.planes_collision(self.flights)
            fuel_collision = CollisionDetector.fuel_has_ran_out_collision(self.flights)

        if altitude_below_zero:
            self.dh.logger.error("Airplane has altitude below zero")
            self.dh.upload_collision(altitude_below_zero)

        if planes_collision:
            self.dh.logger.error("There was a collision of planes")
            self.dh.upload_collision(planes_collision)

        if fuel_collision:
            self.dh.logger.error("The plane ran out of fuel")
            self.dh.upload_collision(fuel_collision)
            


