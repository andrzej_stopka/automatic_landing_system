FROM python:3.10.6

WORKDIR /als

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

EXPOSE 5000
EXPOSE 54321

CMD ["python", "app.py"]

