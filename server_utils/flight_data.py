import random


class FlightData:
    """
    Class representing the data of a flight.

    Attributes:
        client_socket (socket.socket):
        horizontal_position (float):
        vertical_position (float):
        altitude (float):
        fuel (float):
        guidance (str): The current guidance mode of the flight.
        circling_parameters (dict): The parameters used for the 'Circle' guidance mode.
    """
    def __init__(self, client_socket, plane_id, horizontal_position, vertical_position, altitude, fuel, entry_time):
        self.client_socket = client_socket
        self.plane_id = plane_id
        self.horizontal_position = horizontal_position
        self.vertical_position = vertical_position
        self.altitude = altitude
        self.fuel = fuel
        self.entry_time = entry_time
        self.guidance = "Circle"
        self.circling_parameters = self._get_circling_parameters()

    def update_data(self, horizontal, vertical, altitude, fuel):
        self.horizontal_position = horizontal
        self.vertical_position = vertical
        self.altitude = altitude
        self.fuel = fuel

    def _get_circling_parameters(self):
        ### 4500 instead of 5000 to keep distance from borders
        guidance = "Circle"
        horizontal = random.randint(1500, 4500)
        vertical = random.randint(1500, 4500)
        altitude = self.altitude

        return {"guidance": guidance, "horizontal": horizontal, "vertical": vertical, "altitude": altitude}

    @staticmethod
    def plane_with_least_fuel(flights_list):
        """
        Returns a list of the two planes with the least fuel to allow them flying to corridor if it is open.

        Args:
            flights_list (list): A list of FlightData objects representing the flights.

        Returns:
            list: A list containing the FlightData objects of the two planes with the least fuel.
        """
        sorted_flights = sorted(flights_list, key=lambda x: x.fuel)
        return sorted_flights[:2]

