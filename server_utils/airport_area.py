import threading


class AirportArea:
    """
    A class representing the airport area.

    Attributes:
    - landing_point1 (LandingPoint): the first landing point in the airport area
    - landing_point2 (LandingPoint): the second landing point in the airport area
    - corridor1 (Corridor): the first corridor in the airport area
    - corridor2 (Corridor): the second corridor in the airport area
    """
    def __init__(self):
        self.landing_point1 = LandingPoint(-50, 0, 0)
        self.landing_point2 = LandingPoint(50, 0, 0)
        self.corridor1 = Corridor(-500, 0, 500)
        self.corridor2 = Corridor(500, 0, 500)


class LandingPoint:
    """
    A class representing a landing point in the airport area.

    Attributes:
    - horizontal (float): the horizontal position of the landing point
    - vertical (float): the vertical position of the landing point
    - altitude (float): the altitude of the landing point
    """

    def __init__(self, horizontal, vertical, altitude):
        self.horizontal = horizontal
        self.vertical = vertical
        self.altitude = altitude


class Corridor(LandingPoint):
    """
    A class representing a corridor in the airport area.

    Attributes:
    - horizontal (float): the horizontal position of the corridor
    - vertical (float): the vertical position of the corridor
    - altitude (float): the altitude of the corridor
    - lock (threading.Lock): a threading lock used to synchronize access to the corridor
    - open (bool): a flag indicating whether the corridor is open for getting position
    """
    def __init__(self, horizontal, vertical, altitude):
        super().__init__(horizontal, vertical, altitude)
        self.lock = threading.Lock()
        self.open = True
