import json


def send_json(client_socket, data):
    """
    Encodes and sends the given data in JSON format over the specified client socket connection.

    Args:
        client_socket (socket.socket): The socket connection to send the data over.
        data (any): The dictionary data to send in JSON format.
    """
    json_data = json.dumps(data)
    client_socket.send(json_data.encode("utf-8"))


def recv_json(client_socket):
    """
    Receives and decodes data in JSON format over the specified client socket connection.

    Args:
        client_socket (socket.socket): The socket connection to receive data from.

    Returns:
        The decoded data as a Python dictionary.
    """
    try:
        data = client_socket.recv(1024).decode("utf-8")
        data = json.loads(data)
        return data
    except json.decoder.JSONDecodeError:
        return None
