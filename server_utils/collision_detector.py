class CollisionDetector:
    """
    Provides methods to detect collisions between flights.
    """
    altitude_below_zero_planes = []
    planes_collisions_planes = []
    fuel_has_ran_out_planes = []
    
    def _get_collision_info(collision_type, plane_id, plane_horizontal, plane_vertical, plane_altitude, plane_fuel, involved_plane_id):
        collision_info = {
            "collision_type": collision_type, 
            "plane_id": plane_id, 
            "plane_horizontal": plane_horizontal, 
            "plane_vertical": plane_vertical,
            "plane_altitude": plane_altitude, 
            "plane_fuel": plane_fuel, 
            "involved_plane_id": involved_plane_id}
        return collision_info
    

    @staticmethod
    def altitude_below_zero_collision(all_flights):
        """
        Determines if any flight has an altitude below zero.

        Args:
            all_flights (list): A list of Flight objects.

        Returns:
            bool: True if a collision is detected, False otherwise.
        """
        for flight in all_flights:
            if flight.altitude < 0:
                collision_info = CollisionDetector._get_collision_info("altitude below zero", flight.plane_id,
                                      flight.horizontal_position, flight.vertical_position, flight.altitude, flight.fuel, None)
                if not any(collision_info["plane_id"] == collision["plane_id"] for collision in CollisionDetector.altitude_below_zero_planes):
                    CollisionDetector.altitude_below_zero_planes.append(collision_info)
                    return collision_info
        return False

    @staticmethod
    def planes_collision(all_flights):
        """
        Determines if any two flights are colliding.

        Args:
            all_flights (list): A list of Flight objects.

        Returns:
            bool: True if a collision is detected, False otherwise.
        """
        for flight in all_flights:
            for other_flight in all_flights:
                if flight != other_flight:
                    if (abs(flight.vertical_position - other_flight.vertical_position) <= 10
                            and abs(flight.horizontal_position - other_flight.horizontal_position) <= 10
                            and abs(flight.altitude - other_flight.altitude) <= 10):
                        collision_info = CollisionDetector._get_collision_info("planes collision", flight.plane_id,
                                            flight.horizontal_position, flight.vertical_position, flight.altitude, flight.fuel, other_flight.plane_id)
                        if not any(collision_info["plane_id"] == collision["plane_id"] for collision in CollisionDetector.planes_collisions_planes):
                            CollisionDetector.planes_collisions_planes.append(collision_info)
                            return collision_info

        return False

    @staticmethod
    def fuel_has_ran_out_collision(all_flights):
        """
        Determines if any flight has run out of fuel.

        Args:
            all_flights (list): A list of Flight objects.

        Returns:
            bool: True if a collision is detected, False otherwise.
        """
        for flight in all_flights:
            if flight.fuel <= 0:
                collision_info = CollisionDetector._get_collision_info("fuel has ran out", flight.plane_id,
                                    flight.horizontal_position, flight.vertical_position, flight.altitude, flight.fuel, None)
                if not any(collision_info["plane_id"] == collision["plane_id"] for collision in CollisionDetector.fuel_has_ran_out_planes):
                    CollisionDetector.fuel_has_ran_out_planes.append(collision_info)
                    return collision_info
        return False

