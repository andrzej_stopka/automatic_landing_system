import threading
import random
import time
import uuid


class Plane():
    """
    A class that represents a Plane object.

    Attributes:
        flight_direction (str)
        starting_horizontal_position (int)
        starting_vertical_position (int)
        starting_altitude (int)
        horizontal_position (int): The current horizontal position of the plane.
        vertical_position (int): The current vertical position of the plane.
        altitude (int): The current altitude of the plane.
        fuel (int): The amount of fuel the plane has.
        speed (int): The speed of the plane.
    """
    def __init__(self):
        self.id = str(uuid.uuid4())
        self.flight_direction = self._get_starting_direction()
        self.starting_horizontal_position, self.starting_vertical_position, self.starting_altitude, = self._get_starting_position()
        self.horizontal_position = self.starting_horizontal_position
        self.vertical_position = self.starting_vertical_position
        self.altitude = self.starting_altitude
        self.fuel = 10800
        self.speed = 20
        fuel_thread = threading.Thread(target=self.consume_fuel, daemon=True)
        fuel_thread.start()

    def _get_starting_direction(self):
        flight_direction = random.choice(["N", "S", "W", "E"])
        return flight_direction

    def _get_starting_position(self):
        """
        Gets a random starting position for the plane based on its starting direction.

        Returns:
            Tuple[int, int, int]: The starting horizontal position, vertical position, and altitude for the plane.
        """
        match self.flight_direction:
            case "N":
                horizontal = random.randint(0, 5000)
                vertical = -5000
            case "S":
                horizontal = random.randint(0, 5000)
                vertical = 5000
            case "W":
                horizontal = 5000
                vertical = random.randint(0, 5000)
            case "E":
                horizontal = -5000
                vertical = random.randint(0, 5000)

        altitude = random.randint(2000, 5000)
        return horizontal, vertical, altitude

    def circle_over_airport(self, circling_horizontal, circling_vertical, circling_altitude):
        """
        Moves the plane in a circle over the airport waiting for getting corridor command.
        If circling_altitude is different from current plane altitude (change_altitude = True)
        it means that plane must avoid collision with
        another plane.


        """
        time.sleep(0.05)

        change_altitude = False
        if circling_altitude != self.altitude:
            change_altitude = True
            if abs(circling_altitude - self.altitude) <= self.speed / 2:
                self.altitude = circling_altitude
            else:
                if self.altitude > circling_altitude:
                    self.altitude -= self.speed / 2
                elif self.altitude < circling_altitude:
                    self.altitude += self.speed / 2

        vertical_or_horizontal_speed = self.speed if not change_altitude else self.speed / 2

        match self.flight_direction:
            case "N":
                self.vertical_position += vertical_or_horizontal_speed
                if self.vertical_position >= circling_vertical:
                    self.flight_direction = "E"
            case "S":
                self.vertical_position -= vertical_or_horizontal_speed
                if self.vertical_position <= -circling_vertical:
                    self.flight_direction = "W"
            case "W":
                self.horizontal_position -= vertical_or_horizontal_speed
                if self.horizontal_position <= -circling_horizontal:
                    self.flight_direction = "N"
            case "E":
                self.horizontal_position += vertical_or_horizontal_speed
                if self.horizontal_position >= circling_horizontal:
                    self.flight_direction = "S"

        if change_altitude and self.altitude == circling_altitude:
            return True
        return False

    def get_corridor_direction(self, corridor_horizontal, corridor_vertical, corridor_altitude):
        """
        Determines the direction in which the plane should fly to reach the specified corridor.

        """
        # N - north, S - south, E - east, W - west / U - UP, D - DOWN
        direction = ""
        if self.horizontal_position > corridor_horizontal:
            direction += "W"
        if self.horizontal_position < corridor_horizontal:
            direction += "E"
        if self.vertical_position > corridor_vertical:
            direction += "S"
        if self.vertical_position < corridor_vertical:
            direction += "N"
        if self.altitude > corridor_altitude:
            direction += "D"
        if self.altitude < corridor_altitude:
            direction += "U"

        self.flight_direction = direction

    def fly_to_corridor(self, corridor_horizontal, corridor_vertical, corridor_altitude):
        """
        Returns:
        - bool: True if the plane has reached the specified corridor, False otherwise.
        """
        time.sleep(0.05)
        orientations_to_go = 0
        horizontal_value = 0
        vertical_value = 0
        altitude_value = 0

        if "W" in self.flight_direction and self.horizontal_position > corridor_horizontal:
            horizontal_value = -self.speed
            orientations_to_go += 1
        if "E" in self.flight_direction and self.horizontal_position < corridor_horizontal:
            horizontal_value = self.speed
            orientations_to_go += 1
        if "S" in self.flight_direction and self.vertical_position > corridor_vertical:
            vertical_value = -self.speed
            orientations_to_go += 1
        if "N" in self.flight_direction and self.vertical_position < corridor_vertical:
            vertical_value = self.speed
            orientations_to_go += 1
        if "D" in self.flight_direction and self.altitude > corridor_altitude:
            altitude_value = -self.speed
            orientations_to_go += 1
        if "F" in self.flight_direction and self.altitude < corridor_altitude:
            altitude_value = self.speed
            orientations_to_go += 1


        if horizontal_value != 0:
            if abs(self.horizontal_position - corridor_horizontal) <= self.speed/orientations_to_go:
                self.horizontal_position = corridor_horizontal
            else:
                self.horizontal_position += horizontal_value/orientations_to_go

        if vertical_value != 0:
            if abs(self.vertical_position - corridor_vertical) <= self.speed/orientations_to_go:
                self.vertical_position = corridor_vertical
            else:
                self.vertical_position += vertical_value/orientations_to_go

        if altitude_value != 0:
            if abs(self.altitude - corridor_altitude) <= self.speed/orientations_to_go:
                self.altitude = corridor_altitude
            else:
                self.altitude += altitude_value/orientations_to_go

        if orientations_to_go == 0 or (self.horizontal_position == corridor_horizontal and
                                            self.vertical_position == corridor_vertical and
                                            self.altitude == corridor_altitude):
            return True
        return False

    def get_landing_direction(self, landing_horizontal):
        """
        Determines the direction in which the plane should fly to reach the landing point.
        """
        # The vertical is not needed because since the plane is coming in for landing we are sure that its vertical is 0

        if self.horizontal_position > landing_horizontal:
            self.flight_direction = "W"
        elif self.horizontal_position < landing_horizontal:
            self.flight_direction = "E"

    def fly_to_landing_point(self, landing_horizontal):
        """
        Returns:
        - bool: True if the plane has reached the landing point, False otherwise.
        """
        time.sleep(0.05)
        # Speed divided because the plane from the corridor flies from 500 horizontal distance and 500 altitude
        # but 0.7 is for altitude to avoid situation when plane must fly vertically down
        self.speed = 10
        if self.altitude < self.speed * 0.7:
            self.altitude = 0
        else:
            self.altitude -= self.speed * 0.7

        horizontal_speed = self.speed if self.altitude == 0 else self.speed * 0.3
        if abs(self.horizontal_position - landing_horizontal) <= horizontal_speed:
            self.horizontal_position = landing_horizontal
        else:
            if self.flight_direction == "W":
                self.horizontal_position -= horizontal_speed
            elif self.flight_direction == "E":
                self.horizontal_position += horizontal_speed

        if self.horizontal_position == landing_horizontal and self.altitude == 0:
            return True
        return False
    
    def consume_fuel(self):
        """
        Decreases the fuel of the plane by 1 unit every 1 seconds.
        """
        while self.fuel > 0:
            self.fuel -= 1
            time.sleep(1)
