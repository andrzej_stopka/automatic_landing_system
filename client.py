import socket

from server_utils.communication import send_json, recv_json
from server_utils.plane import Plane


class Client:
    def __init__(self, host="0.0.0.0", port=54321):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.plane = Plane()

    def main(self):
        self._connect()
        permission = recv_json(self.sock)
        permission_granted = self._take_permission(permission)
        if not permission_granted:
            print("Permission denied, you must look for another airport")
            self.sock.close()
            return

        plane_position_and_fuel = self._get_plane_details()
        send_json(self.sock, plane_position_and_fuel)

        while True:
            command = recv_json(self.sock)
            if not command:
                print("Server ended connection")
                self.sock.close()
                break
            if command["[COMMAND]"] == "Finished":
                print("Navigation completed successfully")
                self.sock.close()
                break

            guidance = command["[COMMAND]"]["guidance"]
            horizontal = command["[COMMAND]"]["horizontal"]
            vertical = command["[COMMAND]"]["vertical"]
            altitude = command["[COMMAND]"]["altitude"]

            if guidance == "Circle" or guidance == "Circle Avoid":
                avoidance_completed = self.plane.circle_over_airport(horizontal, vertical, altitude)
                plane_position_and_fuel = self._get_plane_details()
                if avoidance_completed:
                    plane_position_and_fuel["[MESSAGE]"] = "Avoidance completed"
                send_json(self.sock, plane_position_and_fuel)

            elif "Corridor" in guidance:

                self.plane.get_corridor_direction(horizontal, vertical, altitude)
                in_corridor = self.plane.fly_to_corridor(horizontal, vertical, altitude)
                plane_position_and_fuel = self._get_plane_details()
                if in_corridor:
                    plane_position_and_fuel["[MESSAGE]"] = "In corridor"
                send_json(self.sock, plane_position_and_fuel)

            elif "Landing" in guidance:
                self.plane.get_landing_direction(horizontal)
                landed = self.plane.fly_to_landing_point(horizontal)
                plane_position_and_fuel = self._get_plane_details()
                if landed:
                    plane_position_and_fuel["[MESSAGE]"] = "Landed"
                send_json(self.sock, plane_position_and_fuel)

    def _connect(self):
        try:
            self.sock.connect((self.host, self.port))
            return True
        except socket.error as error:
            print("[ERROR]:", error.strerror)

    def _take_permission(self, permission):
        if permission["[PERMISSION]"] == "Denied":
            return False
        if permission["[PERMISSION]"] == "Allowed":
            return True

    def _get_plane_details(self):
        data = {"plane id": self.plane.id, 
                "horizontal": self.plane.horizontal_position,
                "vertical": self.plane.vertical_position,
                "altitude": self.plane.altitude,
                "fuel": self.plane.fuel}
        return data


if __name__ == "__main__":
    client = Client()
    client.main()
